﻿using AutoMapper;
using FoodOrder.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using FoodOrder.Domain.Dtos.RestaurantDtos;
using FoodOrder.Domain.Dtos.FoodDtos;
using FoodOrder.Domain.Dtos.CartDtos;
using FoodOrder.Domain.Dtos;

namespace FoodOrder.Domain
{
    public class MappingProfileDomain : Profile
    {
        public MappingProfileDomain()
        {
            RestaurantMapping();
            FoodMapping();
            OrderMapping();
            CartMapping();
        }

        private void CartMapping()
        {
            CreateMap<Cart, CartFoodsDto>();
        }

        private void OrderMapping()
        {
            CreateMap<Cart, OrderDto>()
                .ForMember(x => x.UserName, y => y.MapFrom(m => m.User.UserName));
        }

        private void RestaurantMapping()
        {
            CreateMap<Restaurant, RestaurantDto>();
            CreateMap<RestaurantCreateDto, Restaurant>();
            CreateMap<Restaurant, RestaurantCreateDto>();
            CreateMap<Restaurant, RestaurantInfoDto>();
        }

        private void FoodMapping()
        {
            CreateMap<Food, FoodDto>();
            CreateMap<FoodDto, Food>();
        }
    }
}
