﻿using AutoMapper;
using FoodOrder.Core.Models;
using FoodOrder.DAL;
using FoodOrder.Domain.Dtos.CartDtos;
using FoodOrder.Domain.Dtos.FoodDtos;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FoodOrder.Domain.Dtos.RestaurantDtos;
using UserCartDto = FoodOrder.Domain.Dtos.CartDtos.UserCartDto;

namespace FoodOrder.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<UserCartDto>> GetUserCartAsync(ClaimsPrincipal principal);
        Task<EntityOperationResult<Cart>> AddFoodToCartAsync(int foodId, int restaurantId, ClaimsPrincipal principal);
        Task<EntityOperationResult<Cart>> DeleteAsync(int restaurantId, int foodId, ClaimsPrincipal principal);
        Task<IEnumerable<CartFoodsDto>> GetCartFoodsByRestaurantIdAsync(int restaurantId, ClaimsPrincipal principal);
    }

    public class CartService : ICartService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CartService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public Task<EntityOperationResult<Cart>> AddFoodToCartAsync(int foodId, int restaurantId, ClaimsPrincipal principal)
        {
            throw new NotImplementedException();
        }

        public Task<EntityOperationResult<Cart>> DeleteAsync(int restaurantId, int foodId, ClaimsPrincipal principal)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CartFoodsDto>> GetCartFoodsByRestaurantIdAsync(int restaurantId, ClaimsPrincipal principal)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserCartDto>> GetUserCartAsync(ClaimsPrincipal principal)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var user = await _userManager.GetUserAsync(principal);
                var res = unitOfWork.Carts.GetCart(user.Id).ToList();
                var cart = new List<UserCartDto>();
                if (res.Any())
                {
                    var restaurantIds = res.Select(x => x.RestaurantId).Distinct().ToList();
                    foreach (var restaurantId in restaurantIds)
                    {
                        var foods = res.Where(x => x.RestaurantId == restaurantId).Select(x => new FoodDto()
                        {
                            Count = x.Count,
                            Name = x.Food.Name,
                            Price = x.Food.Price,
                            Id = x.Food.Id,
                            RestaurantId = x.Food.RestaurantId
                        });
                        cart.Add(new UserCartDto()
                        {
                            //Restaurant = Mapper.Map<RestaurantDto>(unitOfWork.Restaurants.GetById((int)restaurantId)),
                            //Foods = foods
                        });
                    }
                }

                return cart;
            }
        }
    }
}
