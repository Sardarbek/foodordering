﻿using AutoMapper;
using FoodOrder.Core.Models;
using FoodOrder.DAL;
using FoodOrder.Domain.Dtos.FoodDtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.Domain.Services
{
    public interface IFoodService
    {
        Task<EntityOperationResult<Food>> CreateAsync(FoodDto foodDto);
        Task<EntityOperationResult<Food>> DeleteAsync(int id);
    }

    public class FoodService : IFoodService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public FoodService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Food>> CreateAsync(FoodDto foodDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var model = unitOfWork.Foods.GetByNameAndRestaurantId(foodDto.Name, foodDto.RestaurantId);
                if (model != null)
                {
                    return EntityOperationResult<Food>.Failure().AddError($"Food {foodDto.Name} already created");
                }

                var food = Mapper.Map<Food>(foodDto);

                await unitOfWork.Foods.AddAsync(food);
                await unitOfWork.CompleteAsync();
                return EntityOperationResult<Food>.Success(food);
            }
        }

        public async Task<EntityOperationResult<Food>> DeleteAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                {
                    var food = unitOfWork.Foods.GetById(id);
                    unitOfWork.Carts.CleanByFoodId(id);
                    unitOfWork.Foods.Delete(food);
                    await unitOfWork.CompleteAsync();
                    return EntityOperationResult<Food>.Success(food);
                }
                catch (Exception e)
                {
                    return EntityOperationResult<Food>.Failure().AddError(e.Message);
                }
            }
        }
    }
}
