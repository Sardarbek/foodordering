﻿using FoodOrder.Domain.Dtos.FoodDtos;
using FoodOrder.Domain.Dtos.RestaurantDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Domain.Dtos
{
    public class OrderDto
    {
        public FoodDto Food { get; set; }
        public RestaurantDto Cafe { get; set; }
        public DateTime OrderTime { get; set; }
        public string UserName { get; set; }


        public int Count { get; set; }
    }
}
