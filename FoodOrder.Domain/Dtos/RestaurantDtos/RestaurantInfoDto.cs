﻿using FoodOrder.Domain.Dtos.FoodDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Domain.Dtos.RestaurantDtos
{
    public class RestaurantInfoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public ICollection<FoodDto> Foods { get; set; }
        public RestaurantInfoDto()
        {
            Foods = new List<FoodDto>();
        }
    }
}
