﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Domain.Dtos.RestaurantDtos
{
    public class RestaurantDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
