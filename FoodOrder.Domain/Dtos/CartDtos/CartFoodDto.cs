﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Domain.Dtos.CartDtos
{
    public class CartFoodsDto
    {
        public int FoodId { get; set; }
        public int Count { get; set; }
    }
}
