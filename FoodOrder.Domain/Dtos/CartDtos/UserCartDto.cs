﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodOrder.Core.Models;
using FoodOrder.Domain.Dtos.FoodDtos;

namespace FoodOrder.Domain.Dtos.CartDtos
{
    public class UserCartDto
    {
        public Restaurant Restaurant { get; set; }

        public IEnumerable<FoodDto> Foods { get; set; }
    }
}
