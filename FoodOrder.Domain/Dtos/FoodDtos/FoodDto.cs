﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Domain.Dtos.FoodDtos
{
    public class FoodDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public int RestaurantId { get; set; }
        public int Count { get; set; }
        public int InCartCount { get; set; }
    }
}
