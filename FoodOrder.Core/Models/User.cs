﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace FoodOrder.Core.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
        }
        public User(string userName) : base(userName)
        {
        }
    }
}
