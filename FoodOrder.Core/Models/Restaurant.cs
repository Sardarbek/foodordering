﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FoodOrder.Core.Models
{
    public class Restaurant : Entity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public ICollection<Food> Food { get; set; }
        public Restaurant()
        {
            Food = new List<Food>();
        }
    }
}
