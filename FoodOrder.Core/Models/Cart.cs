﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Core.Models
{
    public class Cart : Entity
    {
        public int? RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int? FoodId { get; set; }
        public Food Food { get; set; }
        public bool isOrdered { get; set; }
        public int Count { get; set; }
        public DateTime OrderTime { get; set; } = DateTime.Now;
    }
}
