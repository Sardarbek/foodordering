﻿using FoodOrder.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Core.Repositories
{
    public interface ICartRepositoty : IRepository<Cart>
    {
        IEnumerable<Food> GetFoods(int userId);
        Cart GetByRestuarantFoodAndUserId(int restaurantId, int foodId, int userId);
        IEnumerable<Cart> GetCart(int userId, int restaurantId);
        IEnumerable<Cart> GetCart(int id);
        IEnumerable<Cart> GetOrder(int id);
        IEnumerable<Cart> GetOrder();
        void CleanByRestaurantId(int id);
        void CleanByFoodId(int id);
    }
}
