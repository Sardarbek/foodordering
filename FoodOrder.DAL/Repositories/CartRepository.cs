﻿using FoodOrder.Core.Models;
using FoodOrder.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodOrder.DAL.Repositories
{
    public class CartRepository : Repository<Cart>, ICartRepositoty
    {
        public CartRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Carts;
        }

        public void CleanByFoodId(int id)
        {
            var baskets = DbSet.Where(c => c.FoodId == id);
            DbSet.RemoveRange(baskets);
        }

        public void CleanByRestaurantId(int id)
        {
            var orders = DbSet.Where(c => c.RestaurantId == id);
            DbSet.RemoveRange(orders);
        }

        public IEnumerable<Cart> GetCart(int userId, int restaurantId)
        {
            return DbSet.Where(c => c.UserId == userId && c.RestaurantId == restaurantId && !c.isOrdered).Include(c => c.Food);
        }

        public IEnumerable<Cart> GetCart(int userId)
        {
            return DbSet.Where(x => x.UserId == userId && !x.isOrdered).Include(x => x.Food);
        }

        public Cart GetByRestuarantFoodAndUserId(int restaurantId, int foodId, int userId)
        {
            return DbSet.FirstOrDefault(c => c.UserId == userId && c.FoodId == foodId && c.RestaurantId == restaurantId && !c.isOrdered);
        }

        public IEnumerable<Food> GetFoods(int userId)
        {
            return DbSet.Where(c => c.UserId == userId && !c.isOrdered).Include(c => c.Food).Select(c => c.Food);
        }

        public IEnumerable<Cart> GetOrder(int userId)
        {
            return DbSet.Where(c => c.UserId == userId && c.RestaurantId.HasValue && c.FoodId.HasValue && c.isOrdered)
                    .Include(c => c.Restaurant)
                    .Include(c => c.Food)
                    .OrderBy(c => c.OrderTime).ThenBy(c => c.RestaurantId);
        }

        public IEnumerable<Cart> GetOrder()
        {
            return DbSet.Where(c => c.RestaurantId.HasValue && c.FoodId.HasValue && c.isOrdered)
                    .Include(c => c.Restaurant)
                    .Include(c => c.Food)
                    .Include(c => c.User)
                    .OrderBy(c => c.OrderTime).ThenBy(c => c.RestaurantId);
        }
    }
}
