﻿using FoodOrder.Core.Models;
using FoodOrder.Core.Repositories;
using FoodOrder.DAL.DbContext;
using System.Linq;

namespace FoodOrder.DAL.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Restaurant GetByName(string name)
        {
            return DbSet.FirstOrDefault(r => r.Name == name);
        }
    }
}
